const xls2Json = require('xls-to-json');
const ftp = require('basic-ftp');
const path = require('path');
const fs = require('fs');

const MongoClient = require('mongodb').MongoClient;

const OUTPUT_FILE_PATH = 'tmp.json';
const FTP_HOST = 'cygnus.asisa.es';
const FTP_PORT = 222;
const FTP_USER = 'cygnusaxway';
const FTP_PASSWORD = '@xw@yC1gn0s';
const FTP_FILE_PATH = 'Aseg_Reembolsos/in/Asegurados.xlsx';
const EXCEL_FILENAME = 'Asegurados.xlsx';

let db;

async function downloadFile(remoteFilePath, filenameOuput) {
  const client = new ftp.Client();
  client.ftp.verbose = true;

  try {
    await client.access({
      host: FTP_HOST,
      port: FTP_PORT,
      user: FTP_USER,
      password: FTP_PASSWORD,
      secure: true,
      secureOptions: { rejectUnauthorized: false },
    });
    await client.download(fs.createWriteStream(filenameOuput), remoteFilePath);
  } catch (err) {
    console.log('Error downloading excel file', err);
  }

  client.close();
}

function toJSON(filePath) {
  return new Promise((resolve, reject) => {
    xls2Json(
      {
        input: filePath, // inputFilename xls
        output: path.join(__dirname, OUTPUT_FILE_PATH), // output json
      },
      (err, result) => {
        if (err) {
          console.log('toJSON:: Error::', err);
          reject(err);
        } else {
          console.log(`${result.length} users in excel file`);
          resolve(result);
        }
      },
    );
  });
}

function parseToUserDb(excelUser) {
  return {
    nif_insured: excelUser.NIF_ASEGURADO,
    name_insured: excelUser.NOMBRE_ASEGURADO,
    lastname1_insured: excelUser.APELLIDO1_ASEGURADO,
    lastname2_insured: excelUser.APELLIDO2_ASEGURADO,
    address_insured: excelUser.DIRECCION_ASEGURADO,
    province_insured: excelUser.PROVINCIA_ASEGURADO,
    city_insured: excelUser.POBLACION_ASEGURADO,
    cp_insured: excelUser.CODIGO_POSTAL_ASEGURADO,
    birthday_insured:
      excelUser.FECHA_NACIMIENTO_ASEGURADO &&
      new Date(excelUser.FECHA_NACIMIENTO_ASEGURADO),
    num_poliza_insured: excelUser.NUM_POLIZA_ASEGURADO,
    order: excelUser.ORDEN,
    cod_client: excelUser.COD_CLIENTE,
    cod_product: excelUser.COD_PRODUCTO,
    telephone_landline_insured: excelUser.TELEFONO_FIJO_ASEGURADO,
    telephone_mobile_insured: excelUser.TELEFONO_MOVIL_ASEGURADO,
    email_insured:
      excelUser.EMAIL_ASEGURADO && excelUser.EMAIL_ASEGURADO.toLowerCase(),
    nif_owner: excelUser.NIF_TOMADOR,
    name_owner: excelUser.NOMBRE_TOMADOR,
    lastname1_owner: excelUser.APELLIDO1_TOMADOR,
    lastname2_owner: excelUser.APELLIDO2_TOMADOR,
    address_owner: excelUser.DIRECCION_TOMADOR,
    city_owner: excelUser.POBLACION_TOMADOR,
    province_owner: excelUser.PROVINCIA_TOMADOR,
    cp_owner: excelUser.CODIGO_POSTAL_TOMADOR,
    birthday_owner:
      excelUser.FECHA_NACIMIENTO_TOMADOR &&
      new Date(excelUser.FECHA_NACIMIENTO_TOMADOR),
    email_tom: excelUser.EMAIL_TOM && excelUser.EMAIL_TOM.toLowerCase(),
    telephone_landline_tom: excelUser.TELEFONO_FIJO_TOM,
    telephone_mobile_tom: excelUser.TELEFONO_MOVIL_TOM,
    card_number: excelUser.NUMERO_TARJETA,
    sanitarycard_makedate: excelUser.TarjetaSanitaria_FechaEstampacion,
    cod_plastic_type: excelUser.COD_TIPO_PLASTICO,
    des_plastic_type: excelUser.DES_TIPO_PLASTICO,
    des_literal: excelUser.DES_LITERAL,
    cod_bank: excelUser.COD_BANCO,
    cod_branch_office: excelUser.COD_SUCURSAL,
    num_control_digit: excelUser.NUM_DIGITO_CONTROL,
    num_account: excelUser.NUM_NUMERO_CUENTA,
    cod_iban: excelUser.COD_IBAN,
    ind_payment_method: excelUser.IND_MODALIDAD_PAGO,
  };
}

async function insertInsuredToDb(usersInExcel) {
  let count = 0;
  let updatingEmail = 0;

  // test
  // let userFormated = parseToUserDb(usersInExcel[usersInExcel.length - 1]);
  // const result = await db.collection('insured').find({ nif_insured: userFormated.nif_insured, birthday_insured: userFormated.birthday_insured }).toArray();
  // console.log(`${userFormated.nif_insured}:`, result);

  const promises = usersInExcel.map(async user => {
    let userFormated = parseToUserDb(user);

    const result = await db
      .collection('insured')
      .find({
        nif_insured: userFormated.nif_insured,
        birthday_insured: userFormated.birthday_insured,
      })
      .toArray();
    // Crear si no existe asegurado
    if (!result.length) {
      count++;
      return await db.collection('insured').insert(userFormated);
    }

    // Actualizar email de los asegurados no registrados
    if (
      !result[0].password &&
      result[0].email_insured !== userFormated.email_insured
    ) {
      updatingEmail++;
      console.log(
        `actualizando el correo del asegurado sin password (new email ${userFormated.email_insured}):`,
        result[0],
      );
      return await db
        .collection('insured')
        .findOneAndUpdate(
          { _id: result[0]._id },
          { $set: userFormated },
          { upsert: false },
        );
    }

    return Promise.resolve();
  });

  return Promise.all(promises)
    .then(() => {
      console.log(
        `${count} users have been inserted or updated in DB successfully`,
      );
      console.log(`${updatingEmail} emails changed`);
    })
    .catch(error => {
      console.log('Error in insertInsuredToDb::', error);
    });
}

async function migrateUsersFromExcel(filePath) {
  return toJSON(filePath).then(insertInsuredToDb);
}

async function run() {
  await downloadFile(FTP_FILE_PATH, EXCEL_FILENAME);
  MongoClient.connect(
    'mongodb://kabiukiboy:BmdfghsDsg@localhost:27017/bdasisa',
    function(err, database) {
      if (!err) {
        console.log('We are connected to mongo db');
        db = database.db('bdasisa');
        db.collection('insured')
          .count()
          .then(result => {
            console.log(`${result} insureds in db`);

            return migrateUsersFromExcel(`./${EXCEL_FILENAME}`);
          })
          .then(() => {
            process.exit(0);
          });
      }
    },
  );
}

run();
