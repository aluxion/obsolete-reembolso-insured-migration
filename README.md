**Notas**

Este proyecto se encarga de hacer la migración de asegurados para la [web de Reembolso](http://argo.asisa.es) desde un excel continuamente actualizado por Asisa alojado en un servidor FTP (cygnus.asisa.es).

Al ejecutar el script (index.js) este descarga el fichero Excel, lee y parsea la información de los asegurado para posteriormente insertarlos en la base da datos de mongo.

---

## Run script

```bash
 npm start
```
